# 0750 Exercice Git avancé

## Objectifs

* Appliquer les concepts de fork, branche et merge request sur GitLab.
* Compléter un script Python en s'aidant du README.
* Soumettre une merge request pour validation.

## Ressources

* Documentation GitLab : [https://docs.gitlab.com/](https://docs.gitlab.com/)

## Installation

```bash
git config --global user.name "<Prénom> <Nom>"
git config --global user.email "<prenom>.<nom>@edu.vs.ch"
pip install paho-mqtt
```
## Consignes

* Complétez le présent document avec les commandes utilisées, en particuliter les sections **TODO**
* Votre <visa> est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom

## Instructions

### 1. Forker le projet

Rendez-vous sur le dépôt GitLab du projet et cliquez sur le bouton "Fork". Cela créera une copie du projet dans votre espace de travail GitLab.

### 2. Cloner le répertoire distant sur votre machine

- Récupérez le lien https de votre repo nouvellement créé

![clone](img/clone.png)

- Récupérez-le sur votre machine locale
```bash
git clone <url>
```

### 2. Créer une nouvelle branche

Exécutez les commandes suivantes :

```bash
git checkout -b <visa>
```

### 3. Compléter le README

Modifiez le fichier README.md pour chaque étape que vous effectuez. Indiquez les commandes que vous utilisez et les résultats obtenus dans les balises suivantes:
```bash
# TODO saississez les commandes utilisées dans les balises suivantes
```

### 4. Compléter le script Python

Complétez le script `send.py` comme demandé dans p0500. Assurez-vous de tester votre code avant de le commiter.

### 5. Pusher vos modifications

Faites vos push dans la branche `main`

```bash
git add .
git commit -m "feat: updated send.py + README"
git push origin sajirahm
git checkout main
git merge sajirahm
git push origin main
```

### 6. Créer une merge request

Sur GitLab, allez sur votre fork du projet et cliquez sur "Merge Requests". Cliquez sur "New Merge Request". Sélectionnez la branche `main` comme base et votre branche comme source. Ajoutez un titre et une description clairs à votre merge request:
```
<VISA>: 0750 - Travail terminé
```

### 7. Attendre la validation

Patientez que la merge request soit validée par le professeur. Si nécessaire, apportez les modifications demandées.


## Conseils

* Lisez attentivement le README avant de commencer.
* N'hésitez pas à poser des questions au professeur si vous avez besoin d'aide.
* Prenez le temps de tester votre code avant de le commiter.
* Rédigez une merge request claire et concise.
